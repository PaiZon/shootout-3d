using UnityEngine;

public class RayReflectableWeapon : MonoBehaviour, IRayCreator, IReflectionRay, IRayShooter
{
    [SerializeField] private int _reflectionCount = 2;
    [SerializeField] private float _maxRayDistance = 40f;
    
    [SerializeField] private LineRenderer _lineRenderer = null;
    [SerializeField] private Transform _shootPosition = null;
    [SerializeField] private LayerMask _reflectionLayer = 0;
    
    private IDragAction _dragAction;
    private IShootAction _shootAction;
    private IKillable _firstKillable;
    private IParticlePlayer _particlesToPlay;
    private IDieAction _dieAction;
    
    private Ray _ray;
    private RaycastHit _hitInfo;

    private bool _alreadySubscribedOnEvents = false;
    private void Awake()
    {
        if (!_lineRenderer)
            _lineRenderer = GetComponentInChildren<LineRenderer>();
        
        ResetLineRenderer();
        _dragAction = GetComponent<IDragAction>();
        _shootAction = GetComponent<IShootAction>();
        _dieAction = GetComponent<IDieAction>();
    }

    private void OnEnable()
    {
        _dragAction.DragAction += ShootRaycast;
        _shootAction.ShootAction += ShootRaycast;
        _dieAction.OnDie += DisableComponent;
    }

    private void OnDisable()
    {
        _dragAction.DragAction -= ShootRaycast;
        _shootAction.ShootAction -= ShootRaycast;
        _dieAction.OnDie -= DisableComponent;
    }

    public Ray CreateRay(Vector3 origin, Vector3 direction)
    {
        return new Ray(origin, direction);
    }

    public void ShootRaycast()
    {
        RayReflection(_shootPosition.position, transform.forward);
    }
    
    public void RayReflection(Vector3 origin, Vector3 direction)
    {
        ResetLineRenderer();

        var ray = CreateRay(origin, direction);
        float remainingLength = _maxRayDistance;

        for (int i = 0; i < _reflectionCount; i++)
        {
            if (Physics.Raycast(ray, out _hitInfo, remainingLength))
            {
                var killable = _hitInfo.collider.GetComponent<IKillable>();
                var particle = _hitInfo.collider.GetComponent<IParticlePlayer>();
                if ((_reflectionLayer.value & 1 << _hitInfo.collider.gameObject.layer) > 1)
                {
                    InscreaseLineRenderer(_hitInfo.point);
                    remainingLength -= Vector3.Distance(ray.origin, _hitInfo.point);
                    ray = CreateRay(_hitInfo.point, Vector3.Reflect(ray.direction, _hitInfo.normal));
                } 
                else if (killable != null)
                {
                    InscreaseLineRenderer(_hitInfo.point);

                    if(_alreadySubscribedOnEvents) return;
                    SubscribeOnEvents(killable, particle);
                    return;
                }
                else
                {
                    InscreaseLineRenderer(_hitInfo.point);
                }
                UnsubscribeFromEvents();
            }
            else
            {
                UnsubscribeFromEvents();
                InscreaseLineRenderer(ray.origin + ray.direction * remainingLength);
                return;
            }

        }
    }

    private void ResetLineRenderer()
    {
        _lineRenderer.SetPosition(0, _shootPosition.position);
        _lineRenderer.positionCount = 1;
    }

    private void InscreaseLineRenderer(Vector3 position)
    {
        var positionCount = _lineRenderer.positionCount;
        positionCount++;
        _lineRenderer.positionCount = positionCount;
        _lineRenderer.SetPosition(positionCount - 1, position);
    }

    private void SubscribeOnEvents(IKillable killable, IParticlePlayer particle)
    {
        _firstKillable = killable;
        _particlesToPlay = particle;

        _shootAction.ShootAction += _firstKillable.Die;
        _shootAction.ShootAction += ParticlePlay;

        _alreadySubscribedOnEvents = true;
    }

    private void UnsubscribeFromEvents()
    {
        if (_firstKillable != null)
        {
            _shootAction.ShootAction -= _firstKillable.Die;
            _shootAction.ShootAction -= ParticlePlay;
            _particlesToPlay = null;
            _firstKillable = null;
            _alreadySubscribedOnEvents = false;
        }
    }

    private void ParticlePlay() => _particlesToPlay?.PlayParticles(_hitInfo.point);

    private void DisableComponent()
    {
        enabled = false;
        _lineRenderer.gameObject.SetActive(false);
    }
}