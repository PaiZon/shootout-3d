using UnityEngine;
using UnityEngine.UIElements;

public class ParticlePlayer : MonoBehaviour, IParticlePlayer
{
    public GameObject ParticlesToPlay { get => _particlesToPlay; set => _particlesToPlay = value; }
    [SerializeField] private GameObject _particlesToPlay = null;


    public void PlayParticles(Vector3 position)
    {
        var particleSystem = ParticlesToPlay.GetComponent<ParticleSystem>();
        if (particleSystem == null)
            return;
        ParticlesToPlay.SetActive(true);
        ParticlesToPlay.transform.position = position;
    }
}