using System;
using UnityEngine;

public class MouseInputGetter : InputGetter, IDragAction, IShootAction
{
    public event Action DragAction;
    public event Action ShootAction;
    
    [SerializeField] private int _dragMouseButton = 0;
    [SerializeField] private int _shootMouseButton = 1;

    private void Start()
    {
        //Invoking this action for Line Renderer setup
        DragAction?.Invoke();
    }

    public override void GetInput()
    {
        if(!Input.anyKey) return;
        
        if (Input.GetMouseButtonDown(_dragMouseButton))
            StartInputPositon = _camera.ScreenToViewportPoint(Input.mousePosition);
        if (Input.GetMouseButton(_dragMouseButton))
        {
            InputPosition = _camera.ScreenToViewportPoint(Input.mousePosition);
            DragAction?.Invoke();   
        }

        if (Input.GetMouseButtonDown(_shootMouseButton))
        {
            ShootAction?.Invoke();
            SoundManager.Instance.PlaySound(SHOOT_CLIP_NAME);
        }
    }
}