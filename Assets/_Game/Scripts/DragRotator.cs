﻿using UnityEngine;

public class DragRotator : MonoBehaviour
{

    [SerializeField] private float _rotationSensitivity = 10f;

    private IInputGetter _inputGetter;
    private IDragAction _dragAction;

    private float _screenWidth;

    private void Awake()
    {
        _inputGetter = GetComponent<IInputGetter>();
        _dragAction = GetComponent<IDragAction>();

        _screenWidth = Screen.width;
    }

    private void OnEnable()
    {
        _dragAction.DragAction += RotatePlayer;
    }

    private void OnDisable()
    {
        _dragAction.DragAction -= RotatePlayer;
    }

    private void RotatePlayer()
    {
        Vector2 newMousePosition = _inputGetter.InputPosition;
        var localEulerAngles = transform.localEulerAngles;
        localEulerAngles += CalculateRotation(newMousePosition.x, transform.up);
        transform.localEulerAngles = localEulerAngles;
        _inputGetter.StartInputPositon = newMousePosition;
    }

    private Vector3 CalculateRotation(float xPosition, Vector3 Axis)
    {
        float newYRotation = (xPosition - _inputGetter.StartInputPositon.x);
        return Axis * (newYRotation * _screenWidth * _rotationSensitivity * Time.deltaTime);
    }
}