﻿using System;

public interface IDragAction
{
    event Action DragAction;
}