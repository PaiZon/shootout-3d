﻿using UnityEngine;

public interface IRayCreator
{
    Ray CreateRay(Vector3 origin, Vector3 direction);
}