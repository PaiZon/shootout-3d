﻿using UnityEngine;

public interface IInputGetter
{
    Vector3 InputPosition { get; set; }
    Vector3 StartInputPositon { get; set; }
    void GetInput();
}