﻿using System;
using UnityEngine;

public interface IParticlePlayer
{
    GameObject ParticlesToPlay { get; set; }
    void PlayParticles(Vector3 position);
}