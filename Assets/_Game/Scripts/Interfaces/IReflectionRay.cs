﻿using UnityEngine;

public interface IReflectionRay
{
    void RayReflection(Vector3 origin, Vector3 direction);
}