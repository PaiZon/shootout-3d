﻿using System;

public interface IDieAction
{
    event Action OnDie;
}