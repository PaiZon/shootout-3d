﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader Instance;

    [SerializeField] private CanvasGroup _fadeImage = null;
    [SerializeField] private Button _reloadSceneButton;

    private int _currentSceneIndex = 0;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        _reloadSceneButton.onClick.AddListener(() => ReloadScene(1f));
    }

    public void ReloadScene(float delay)
    {
        StartCoroutine(SceneLoadDelay(delay));
    }

    public void LoadNextScene(float delay)
    {
        if (++_currentSceneIndex >= SceneManager.sceneCountInBuildSettings)
        {
            _currentSceneIndex = 0;
        }

        StartCoroutine(SceneLoadDelay(delay));
    }

    private IEnumerator SceneLoadDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(_currentSceneIndex);
    }
}
