﻿using System;
using UnityEngine;

public class TouchInputGetter : InputGetter, IDragAction, IShootAction
{
    public event Action DragAction;
    public event Action ShootAction;

    [SerializeField] private float _tapTimeThreshold = 0.2f;

    private bool _touchDidMove = false;
    private float _timeTouchBegan = 0f;

    private const float ALLOWED_CLICK_SPACE = 0.85f;

    private void Start()
    {
        //Invoking this action for Line Renderer setup
        DragAction?.Invoke();
    }
    
    public override void GetInput()
    {
        if(Input.touchCount <= 0) return;
        
        Touch touch = Input.GetTouch(0);
        //Guard if you click in button characters will not shoot
        if(touch.position.y > Screen.height * ALLOWED_CLICK_SPACE) return;
        
        if (touch.phase == TouchPhase.Began)
        {
            StartInputPositon = _camera.ScreenToViewportPoint(touch.position);
            _timeTouchBegan = Time.time;
        }
        if (touch.phase == TouchPhase.Moved)
        {
            InputPosition = _camera.ScreenToViewportPoint(touch.position);
            DragAction?.Invoke();
            _touchDidMove = true;
        }

        if (touch.phase == TouchPhase.Ended)
        {
            float tapTime = Time.time - _timeTouchBegan;
            if (tapTime <= _tapTimeThreshold && !_touchDidMove)
            {
                ShootAction?.Invoke();
                SoundManager.Instance.PlaySound(SHOOT_CLIP_NAME);
            }
            _touchDidMove = false;
        }
    }

}
