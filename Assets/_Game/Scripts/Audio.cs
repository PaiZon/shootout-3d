﻿using UnityEngine;

[System.Serializable]
public class Audio
{
    public string name;
    public AudioClip clip;
    public AudioSource source;
    [Range(0, 1)] public float volume;
    [Range(0, 3)] public float pitch;
    public bool playOnAwake;
    public bool loop;
    public bool randomPitch;
}