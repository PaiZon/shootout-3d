﻿using System;
using System.Collections;
using UnityEngine;

public class DieProvider : MonoBehaviour, IKillable, IDieAction
{
    public event Action OnDie;
    
    [SerializeField] private GameObject _ragdollModel = null;
    [SerializeField] private GameObject _normalModel = null;

    [SerializeField] private CharacterType _characterType = CharacterType.Player;
    
    private bool _isDead = false;
    private Collider _collider = null;

    private const string DIE_SOUND_NAME = "Die";

    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }
    
    public void Die()
    {
        if(_isDead)
            return;
        _isDead = true;
        StartCoroutine(DieDelay());
    }
    
    // Coroutine delaying death so the enemies that points on each other can kill themselves 
    private IEnumerator DieDelay()
    {
        SoundManager.Instance.PlaySound(DIE_SOUND_NAME);
        yield return new WaitForSeconds(0.1f);
        OnDie?.Invoke();
        _collider.enabled = false;
        _normalModel.SetActive(false);
        _ragdollModel.SetActive(true);
        if (_characterType == CharacterType.Player)
           SceneLoader.Instance.ReloadScene(1f);
        else if (_characterType == CharacterType.Boss)
            SceneLoader.Instance.LoadNextScene(2f);
    }
}