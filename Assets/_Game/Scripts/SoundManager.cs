﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    [SerializeField] private Audio[] _audioArray = Array.Empty<Audio>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
            Destroy(gameObject);
    }
    public void PlaySound(string audioName)
    {
        foreach (var sound in _audioArray)
        {
            if (audioName == sound.name)
            {
                sound.source.clip = sound.clip;
                sound.source.volume = sound.volume;
                sound.source.pitch = sound.randomPitch ? Random.Range(.8f, 3f) : sound.pitch;
                if (sound.playOnAwake)
                {
                    sound.source.playOnAwake = true;
                }
                if (sound.loop)
                {
                    sound.source.loop = true;
                }
                sound.source.Play();
            }
        }
    }
}