﻿using UnityEngine;

public abstract class InputGetter : MonoBehaviour, IInputGetter
{
    public Vector3 InputPosition { get; set; }
    public Vector3 StartInputPositon { get; set; }
    public abstract void GetInput();


    [SerializeField] protected Camera _camera = null;

    protected const string SHOOT_CLIP_NAME = "Shoot";
    private IDieAction _dieAction;
    private void Awake()
    {
        if (!_camera)
            _camera = Camera.main;
        _dieAction = GetComponent<IDieAction>();
    }

    private void OnEnable()
    {
        _dieAction.OnDie += Disable;
    }
    private void OnDisable()
    {
        _dieAction.OnDie += Disable;
    }

    private void Update()
    {
        GetInput();
    }
    
    private void Disable()
    {
        enabled = false;
    }
}